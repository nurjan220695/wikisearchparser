import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.jsoup.Jsoup;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Scanner;


class WikiSearchParser {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        System.out.println("- Введите текст для поиска по википедии: ");
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();

        String url = "https://ru.wikipedia.org/w/api.php?action=query&format=xml&srlimit=10&list=search&srsearch=" + URLEncoder.encode(a, "UTF-8");
        URL address = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) address.openConnection();

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        String str = response.toString();

        InputStream is = new ByteArrayInputStream(str.getBytes());

        ArrayList<Link> linkArr = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(is);

        doc.getDocumentElement();

        NodeList itemElements = doc.getElementsByTagName("p");

        for (int i = 0; i < itemElements.getLength(); i++) {
            Node nNode = itemElements.item(i);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                int id = i + 1;
                String title = eElement.getAttribute("title");

                linkArr.add(new Link(title,
                        id,
                        "https://ru.wikipedia.org/wiki/" + URLEncoder.encode(title, "UTF-8").replace("+", "_")));
            }
        }

        for (int i = 0; i < linkArr.size(); i++) {
            System.out.println(linkArr.get(i).getId() + ")" + linkArr.get(i).getText() + " (" + linkArr.get(i).gettUrl() + ")");

        }
        System.out.println();
        System.out.println("- Выберите статью для отображения: ");
        Scanner s2 = new Scanner(System.in);
        int b = s2.nextInt();

        System.out.println();

        System.out.println("- Контент: ");
        for (int i = 0; i < linkArr.size(); i++) {
            if (b == linkArr.get(i).getId()) {

                String url2 = linkArr.get(i).gettUrl();
                org.jsoup.nodes.Document doc2 = Jsoup.connect(url2).get();
                String reg = "[0-9]";


                org.jsoup.select.Elements itemElements2 = doc2.getElementsByClass("mw-parser-output");


                Elements paragraphs = itemElements2.select("p, h1, h2, h3, ul, li");
                for (org.jsoup.nodes.Element itemElement : paragraphs) {
                    if (itemElement.is("h2")) {
                        System.out.println("______________________________________________________________________________");
                    }

                    System.out.println(itemElement.text());
                }
            }
        }
    }
}








