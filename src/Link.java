
public class Link {

    private String text;
    private String tUrl;
    int id;

    public Link(String text, int id, String tUrl) {
        this.text=text;
        this.id=id;
        this.tUrl=tUrl;
    }


    public String getText() {
        return text;
    }

    public String gettUrl() {
        return tUrl.replaceAll("<span class=\"searchmatch\">", "").replaceAll("</span>","");
    }

    public int getId() {
        return id;
    }


}
